<?php

namespace App\Orchid\Screens;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create comment';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Manage comment';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if ($this->exists) {
            $this->body = "Edit {$comment->body} comment";
        }
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('comment.body')
                    ->title('Body')
                    ->placeholder('Enter comment body')->required(),

                Relation::make('Author')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id')->required(),

                Quill::make('comment.body')
                    ->title('Comment content')->required()
            ])
        ];
    }


    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('Successfully create');
        return redirect()->route('platform.comments.list');
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('Delete')
            : Alert::warning('No deleted!');
        return redirect()->route('platform.comments.list');
    }
}
