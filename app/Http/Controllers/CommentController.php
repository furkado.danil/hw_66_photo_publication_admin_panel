<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Photo;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->photo_id = $request->input('photo_id');
        $comment->user_id = $request->user()->id;
        $comment->save();

        return redirect()->route('photo', compact('comment'));
    }


    /**
     * @param Request $request
     * @param int $comment_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, int $comment_id)
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);

        $comment = Comment::findOrFail($comment_id);
        $comment->update($request->all());

        return redirect()->route('photo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();

        return redirect()->route('photo');
    }
}
