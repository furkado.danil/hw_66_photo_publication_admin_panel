<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PhotoController@index')->name('photo');

Route::resource('photo', 'PhotoController')->only('index');

Route::resource('comments', 'CommentController')->only('store', 'update', 'destroy');

Route::resource('user', 'UserController')->only('index', 'store', 'destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
