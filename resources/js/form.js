$(document).ready(function () {
    $('.edit-comment').click(function (event) {
        const commentId = $(this).attr('data-comment-id');
        $(`#comment-e-f${commentId}`).removeClass('edit-form');
        $(this).hide();
    });
});