@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger mt-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row mt-5">
    @foreach($photos as $photo)
        <!-- Modal -->
            <div class="modal fade" id="exampleModal-{{$photo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Автор: {{$photo->user->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img src="{{asset( '/storage/'. $photo->picture)}}" width="466" alt="{{$photo->picture}}">
                        </div>
                        <div class="modal-footer">
                            <div class="scrollspy-example">
                                @csrf
                                @foreach($photo->comments as $comment)
                                    <x-comment :comment="$comment"></x-comment>
                                @endforeach
                            </div>
                            @if(Auth::check())
                                <form action="{{route('comments.store')}}" method="post">
                                    @csrf
                                    <div class="form-row">
                                        @csrf
                                        <div class="col">
                                            <input type="hidden" id="photo_id-{{$photo->id}}" name="photo_id" value="{{$photo->id}}">
                                            <div class="form-group">
                                                <textarea name="body" type="text" class="form-control body" placeholder="Напите что небудь" rows="0" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary click-comment-btn mt-4">Коментировать</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        <!-- End Modal -->


            <figure class="figure ml-2">
                <button type="button" class="btn btn-light" data-toggle="modal"  data-target="#exampleModal-{{$photo->id}}">
                    <img src="{{asset( '/storage/'. $photo->picture)}}" class="rounded mx-auto d-block" width="200" height="200" alt="{{$photo->picture}}">
                </button>
                <div class="col">
                    <span class="badge badge-info" style="cursor: pointer" title="Количество коментариев">{{$photo->comments->count()}}</span>
                </div>
            </figure>
        @endforeach
    </div>


@endsection