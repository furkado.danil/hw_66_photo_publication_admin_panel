<div class="comment-block-{{$comment->id}}" id="delete-comment-{{$comment->id}}">
    @can('delete', $comment)
        <form action="{{route('comments.destroy', ['comment' => $comment])}}" method="post">
            @method('DELETE')
            @csrf
            <button class="delete" type="submit" aria-hidden="true">&times;</button>
        </form>

    @endcan
    <h5 class="h5 g-color-gray-dark-v1 mb-0">Автор: {{$comment->user->name}}</h5>
    <p>{{$comment->body}}</p>
    @can('update', $comment)
        <button data-comment-id="{{$comment->id}}" id="comment-{{$comment->id}}" class="btn btn-sm btn-primary edit-comment">Изменить</button>
    @endcan
    <form action="{{route('comments.update', ['comment' => $comment])}}" method="post">
        @method('PUT')
        <div id="comment-e-f{{$comment->id}}" class="edit-form">
            @csrf
            <div class="form-group">
                <label for="body-edit-{{$comment->id}}">Comment</label>
                <textarea name="body" class="form-control" id="body-edit-{{$comment->id}}"  rows="3" required>{{$comment->body}}</textarea>
            </div>
            <button  type="submit" class="btn btn-outline-primary btn-sm btn-block update-comment">Изменить коментарий</button>
        </div>
    </form>
</div>
